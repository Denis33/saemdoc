.. _asalae:

.. _asalae_accueil:


Module - gestion de l'archivage et de la conservation
-----------------------------------------------------

Fonctionnalités
~~~~~~~~~~~~~~~~
Le module offre de base la possibilité de valider le versement des paquets d'informations soumis par le module de gestion des processus d'archivage et de les transformer en paquets d'information à archiver.

Une fois connecté, l'utilisateur habilité peut gérer le versement, l'élimination et la gestion préventive des archives électronique sous sa responsabilité.

.. _asalae_transfert:

1 Accepter un transfert dans Asalae
===================================

Les dossiers transférés apparaissent dans « Mes Transferts à traiter » ou « Transferts non conformes » quelques secondes après avoir été transférés depuis la GED SAS.

1. Se placer dans le sous menu Transferts  Mes transferts à traiter (ou Transferts non conformes)
2. Cocher la case à gauche de la ligne du transfert concernée
3. Choisir « Accepter » dans la liste déroulante
4. Cliquer sur le bouton « Accepter » 


.. image:: ../images/validerversementAsalae.png
   :width: 987px
   :height: 377px
   :scale: 80%
   :alt: confirmation validation versement as@lae 
   :align: center

5. Répéter l’opération autant de fois que nécessaire

.. note::
Une tâche planifiée au niveau de la GED est dédiée à la récupération des messages SEDA émis par Asalae. Il faudra attendre quelques minutes pour que le message d’acceptation remonte au niveau de la GED SAS. A ce moment-là le dossier de versement aura une couleur verte et le statut versé.


6 Traitement de la restitution dans Asalae
La demande de restitution apparaît dans la validation des demandes de restitution.
 
Les actions à effectuer pour accepter la restitution sont similaires à celles du transfert. Ce point a été abordé au paragraphe 9.4.

Une tâche planifiée au niveau de la GED est dédiée à la récupération des messages SEDA émis par Asalae. Il faudra attendre quelques minutes pour que le message d’acceptation remonte au niveau de la GED SAS. A ce moment-là l’archive aura une couleur violette et le statut « restituée ».

10 Workflow d’élimination
Une demande d’élimination peut être effectuée pour les archives versées dont la DUA est expirée.
Pour qu’une demande d’élimination puisse être traitée dans Asalae, l’outil de conversion de document Cloudoo doit être correctement paramétré.

10.1   Elimination depuis un site versant
=============================================

L’action d’élimination peut être lancée de trois manières différentes :

1. Depuis la dashlet des traitements, un icône   est disposé à côté des actions possibles.
2. Depuis la dashlet « mes Actions », les dossiers éliminables sont affichés


3. Depuis l’espace documentaire, les filtres proposés permettent de visualiser les dossiers versés mais ne prennent pas compte de l’expiration des DUA

Paramétrage initial
~~~~~~~~~~~~~~~~~~~~

1 Synchronisation dans as@lae
=============================

Les URLS OAI-PMH des éléments à synchroniser doivent être définies sous forme de référentiel extérieurs en accédant au menu Administration Technique >Référentiel Extérieur.

.. image:: ../images/synchroAsalae.png
   :width: 868px
   :height: 202px
   :scale: 80%
   :alt: paramétrage synchro asalae
   :align: center

La synchronisation des différents éléments peut être lancée individuellement en cliquant sur l’icône  loupe présentée dans l’interface puis en exécutant une des deux actions suivantes proposées en bas de page de visualisation du référentiel :

.. image:: ../images/synchroAsalae.png
   :width: 659px
   :height: 49px
   :scale: 80%
   :alt: paramétrage synchro asalae
   :align: center

.. index:: ark

La « mise à jour du référentiel » effectue alors une mise à jour différentielle en synchronisant uniquement les éléments modifiés depuis la dernière synchronisation. Cette fonctionnalité récupère tous les éléments du référentiel quelque soit leur date de modification.
Une tâche planifiée est également disponible depuis le menu  Administration Technique >Tâches planifiées afin d’effectuer une sychronisation complète de l’ensemble des référentiels extérieurs actifs.

4.2   Circuits de traitement
============================

Les circuits de traitement par défauts sont tous constitués d’une étape de contrôle et d’une étape de validation faisant uniquement intervenir l’utilisateur admin.
Tous les circuits de traitements par défaut doivent être modifiés de manière à comporter une seule étape concurrente avec une seule composition de type « service d’archives ». Cette action ne doit être effectuée qu’une seule fois avant de commencer à verser des éléments dans Asalae.


4.2.1 Traitement des transferts d’archives SAEM
===============================================

1. Aller dans Administration >Circuits de traitement
2. Cliquer sur le lien en base de page « Ajouter un circuit de traitement»
3. Choisir « Traitement des transferts d’archives » dans le Type du circuit
4. Renseigner le formulaire comme ci-dessous

.. image:: ../images/traitementArchives.png
   :width: 588px
   :height: 297px
   :scale: 80%
   :alt: paramétrage traitement archives
   :align: center

5. Valider

4.2.2 Etapes et composition du circuit des transferts
=====================================================

1. Aller dans Administration >Circuits de traitement
2. Cliquer sur l’icône   au niveau du circuit de traitement
3. Cliquer sur le lien « Ajouter une étape»
4. Renseigner le formulaire comme ci-dessous

.. image:: ../images/circuitTraitement.png
   :width: 781px
   :height: 271px
   :scale: 80%
   :alt: circuit traitement archives
   :align: center

5. Cliquer sur valider
6. Cliquer sur l’icône   pour composer l’étape 
7. Cliquer sur le lien « Ajouter une composition»
8. Choisir « Service d’archives » au niveau du type de composition

.. image:: ../images/compositionArchives.png
   :width: 184px
   :height: 90px
   :scale: 80%
   :alt: type composition archives
   :align: center

9. Valider



4.2.3 Modification du circuit « traitement des demandes de communication »
=====================================================

1. Aller dans Administration >Circuits de traitement
2. Cliquer sur le drapeau  au niveau du circuit « Traitement des demandes de communication »
3. Cliquer sur l’icône  
4. Editer le type 
5. Choisir « Service d’archives » au niveau du type de composition

.. image:: ../images/compositionEtapeCircuittraitement.png
   :width: 402px
   :height: 152px
   :scale: 80%
   :alt: type composition étapes
   :align: center

6. Valider

4.2.4 Modification du circuit « traitement des demandes d’éliminations »
=====================================================

1. Aller dans Administration >Circuits de traitement
2. Cliquer sur le drapeau  au niveau du circuit « Traitement des demandes d’élimination»
3. Cliquer sur l’icône   au niveau de l’étape « Accord du service producteur »
4.   Supprimer la composition de type « Service producteur » en cliquant sur   
5.   Cliquer sur le lien « Retour à la liste des étapes »
6.   Supprimer l’étape « Accord du service producteur »
7.   Modifier l’étape « Validation » en cliquant sur l’icône  
8.   Editer le type de composition en cliquant sur  
9.   Choisir « Service d’archives » au niveau du type de composition

.. image:: ../images/compositionEtapeCircuittraitement.png
   :width: 402px
   :height: 152px
   :scale: 80%
   :alt: type composition étapes
   :align: center 

10.   Valider

4.2.5 Modification du circuit « traitement des demandes de restitution »
========================================================================

1.   Aller dans Administration >Circuits de traitement
2.   Cliquer sur le drapeau  au niveau du circuit « Traitement des demandes de retitutiob»
3.   Cliquer sur l’icône   au niveau de l’étape « Contrôle »
4.   Supprimer la composition de type « Utilisateur as@lae» en cliquant sur   
5.   Cliquer sur le lien « Retour à la liste des étapes »
6.   Supprimer l’étape « Contrôle »
7.   Modifier l’étape « Validation » en cliquant sur l’icône  
8.   Editer le type de composition en cliquant sur  
9.   Choisir « Service d’archives » au niveau du type de composition

.. image:: ../images/compositionEtapeCircuittraitement.png
   :width: 402px
   :height: 152px
   :scale: 80%
   :alt: type composition étapes
   :align: center

10.   Valider

4.2.6 Modification du circuit « traitement des transferts d’archives »
======================================================================

1.   Aller dans Administration >Circuits de traitement
2.   Cliquer sur le drapeau  au niveau du circuit « Traitement des transferts d’archives»
3.   Cliquer sur l’icône   au niveau de l’étape « Contrôle »
4.   Supprimer la composition de type « Utilisateur as@lae» en cliquant sur   
5.   Cliquer sur le lien « Retour à la liste des étapes »
6.   Supprimer l’étape « Contrôle »
7.   Modifier l’étape « Validation » en cliquant sur l’icône  
8.   Editer le type de composition en cliquant sur  
9.   Choisir « Service d’archives » au niveau du type de composition

.. image:: ../images/compositionEtapeCircuittraitement.png
   :width: 402px
   :height: 152px
   :scale: 80%
   :alt: type composition étapes
   :align: center

10.   Valider


4.3   Collectivités et autorité d'archivage dans Asalae
=======================================================

Asalae gère de multiples collectivités depuis la version 1.6.1.

Une collectivité dans Asalae correspond à une autorité administrative dans le référentiel.
Pour chaque autorité administrative du référentiel possédant des unités administratives publiées, une collectivité doit être créée manuellement dans asalae. A chaque fois qu’une unité administrative est publiée dans le référentiel il faut créer la collectivité associée à son autorité d’archivage si elle n’existe pas dans Asalae.

4.3.2 Création d’une collectivité
=================================

1. Aller sous le menu Administration > collectivité-organismes
2. Cliquer sur le lien Ajouter une collectivité-organisme
3. Renseigner le nom et l’identifiant unique. L’identifiant unique doit être sous la forme {url_referentiel}/ {id_autorite_administrative}.
Il faut remplacer {url_referentiel} par l’url de base du  référentiel (ex : http://saem-ref-va.cg33.fr) et {id_autorite_administrative} par le l’identifiant de l’autorité administrative (ex : 35297). Ce qui donnera l’identifiant  http://saem-ref-va.cg33.fr/35297 


.. image:: ../images/creationCollectivite.png
   :width: 184px
   :height: 90px
   :scale: 80%
   :alt: création collectivité
   :align: center

Pour connaître l’identifiant numérique, il faut aller à la page du référentiel de l’autorité. L’identifiant est affiché en bas de page. Il est entouré en rouge dans la capture ci-dessous.

.. image:: ../images/IdentificationCollectivite.png
   :width: 998px
   :height: 323px
   :scale: 80%
   :alt: identification collectivité
   :align: center



4.3.3 Paramétrage des droits
============================

1. Aller sous le menu Administration > collectivité-organismes
2. Cliquer sur le bouton de modification de la collectivité  dans la liste des actions
3. Cliquer sur l’onglet  "droits"
4. Cocher toutes les cases puis valider

4.3.4 Création des rôles « archival » et « producer »
=====================================================

Pour chaque collectivité, un rôle nommé « archival » et un rôle nommé « producer » doivent être créés. Le rôle archival devra contenir les droits associés à un utilisateur du service archive et le rôle « producer » les droits associés à un utilisateur du service versant.Le paragraphe suivant décrit les étapes permettant la création d’un rôle.
1. Aller sous le menu Administration > Rôles utilisateur
2. Cliquer sur le lien Ajouter un rôle utilisateur
3. Sélectionner la collectivité dans la liste puis valider
4. Renseigner le nom du rôle et sa description puis valider


.. image:: ../images/creationAdminAsalae.png
   :width: 727px
   :height: 204px
   :scale: 80%
   :alt: creation rôle admin archives
   :align: center

4.3.5 Création des utilisateurs
=================================

Un compte utilisateur sera créé par Asalae pour chaque contact référent d’une unité administrative ayant le rôle archivistique « archive » ou « producteur ». La convention de nommage des identifiants utilise la première lettre du prénom suivie du nom. Par exemple le contact référent « Pierre Daniel » aura l’identifiant de connexion « pdaniel ». Le mot de passe initial est identique à l’identifiant de connexion. Lors de sa première connexion Pierre Daniel pourra utiliser le mot de passe « pdaniel ». 


.. image:: ../images/creationUserAsalae.png
   :width: 732px
   :height: 385px
   :scale: 80%
   :alt: creation user asalae
   :align: center

.. warning:: 
Une réinitialisation du mot de passe sera demandée à la première connexion.

4.4   Services dans Asalae
==========================
Prérequis :

* Service d’archive : le service d’archive devra avoir une date de fin non échue lors du versement.
* Service versant: dans la version actuelle d’Asalae, un service versant doit être également producteur pour que ses versements soient acceptés. Si tel n’est pas le cas il faut ajouter le rôle producteur à l’unité administrative correspondant au service au niveau du référentiel et synchroniser les services versants dans  Asalae.
* Connecteurs : il faut sélectionner un connecteur pour chaque service, quel que soit son rôle.
1. Aller dans le menu Administration SEDA > Acteurs SEDA
2. Cliquer sur l’icône de modification  du service
3. Sélectionner un connecteur
4. Valider

Types de composition : 

Les compositions sont utilisés dans les différents circuits de traitement des archives. Une compsition est soit une adresse email, soit un parapheur électronique ou un utilisateur d’asa@lae. 
Il faut ajouter au moins une composition à chaque service quelque soit son rôle.

1. Aller dans le menu Administration SEDA > Acteurs SEDA
2. Cliquer sur l’icône de modification 
3. Aller dans l’onglet « Traitements »
4. Sélectionner « Utilisateur as@lae » au niveau du type de composition à ajouter 

.. image:: ../images/compositionTypeUtilisateurAsalae.png
   :width: 220px
   :height: 48px
   :scale: 80%
   :alt: creation type composition user asalae
   :align: center

.. warning::
Les opérations suivantes (5,6,7) doivent être effectuée pour chaque utilisateur du service archive.

5. Cliquer sur le bouton +
6. Choisir un utilisateur
7. Sélectionner « Visa » au niveau du type de validation

.. image:: ../images/typeVisaAsalae.png
   :width: 417px
   :height: 210px
   :scale: 80%
   :alt: creation type visa user asalae
   :align: center

8. Valider

4.5   Accords de versement
==========================

Un accord de versement est obligatoire pour l’acceptation d’un versement. S’il n’est pas spécifié au moment du versement, c’est l’accord de versement par défaut qui sera pris en compte. 
Il est donc nécessaire d’en créer un car aucun accord de versement n’est spécifié lors des transferts effectués depuis la GED SAS. Les étapes nécessaires à la création d’un accord de versement sont précisées ci-dessous.

Création dans Asalae

1. Aller dans le menu Administration SEDA>Accords de versement
2. Cliquer sur “Ajouter un Accord de versement”
3. Remplir les champs obligatoires


.. image:: ../images/ajoutAccordVersement.png
   :width: 975px
   :height: 852px
   :scale: 80%
   :alt: creation accord versement asalae
   :align: center

* identifiant correspond au champ de la balise "Archival agreement" du profil SEDA (ne pas mettre ni d'espace ni d'accent)
* sélectionner un service archive
* sélectionner les services versants (CTRL+A)
* Choisir le volume de stockage

4. Au niveau de l’onglet Contrôles sous « Pièces jointes » cocher « Validation des pièces … »

.. image:: ../images/validationPieceJointe.png
   :width: 921px
   :height: 194px
   :scale: 80%
   :alt: paramétre validité pièce jointe asalae
   :align: center


5. Valider
6. Editer l’accord de versement créé

.. _warning::
 (il faut enregistrer l’accord une première avant d’effectuer cette étape, sinon la case cochée ne sera pas prise en compte)

7. Cocher « Autoriser tous les producteurs… » 

8. Valider

.. _note:: Reporter l’accord de versement dans le site service archive associé dans la GED SAS

Traitement de la restitution dans Asalae
=========================================

La demande de restitution apparaît dans la validation des demandes de restitution.
Les actions à effectuer pour accepter la restitution sont similaires à celles du transfert. Ce point a été abordé au paragraphe :ref:`_asalae_transfert`.


Une tâche planifiée au niveau de la GED est dédiée à la récupération des messages SEDA émis par Asalae. Il faudra attendre quelques minutes pour que le message d’acceptation remonte au niveau de la GED SAS. A ce moment-là l’archive aura une couleur violette et le statut « restituée ».

11 Traitement de l’élimination dans Asalae
===========================================

Les demandes d’élimination à traiter sont accessibles à travers le menu « Validation des demandes d’élimination ».

Les actions à effectuer pour accepter la restitution sont similaires à celles du transfert. Ce point a été abordé au paragraphe :ref:`_asalae_transfert`.


Une tâche planifiée au niveau de la GED est dédiée à la récupération des messages SEDA émis par Asalae. Il faudra attendre quelques minutes pour que le message d’acceptation remonte au niveau de la GED SAS. A ce moment-là  l’archive aura une couleur rouge et le statut « éliminée ».

12   Traitement de la communication dans Asalae
===================================================

La demande de restitution apparaît dans la validation des demandes de communication.

.. image:: ../images/traitementComAsalae.png.png
   :width: 768px
   :height: 124px
   :scale: 80%
   :alt: paramétre validité pièce jointe asalae
   :align: center


Les actions à effectuer pour accepter la communication sont similaires à celles du transfert. 

Une tâche planifiée au niveau de la GED est dédiée à la récupération des messages SEDA émis par Asalae. Il faudra attendre quelques minutes pour que le message d’acceptation remonte au niveau de la GED SAS. A ce moment-là les éléments communiqués seront déposés dans le dossier Communication du service versant. Les dossiers communiqués seront présentés sur le tableau de suivi des traitements avec l’icône . 
 
L’icône   est cliquable et renvoie au dossier contenant les éléments communiqués.