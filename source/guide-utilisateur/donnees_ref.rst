.. _donnees_ref:

.. index:: accueil-donnees-ref


Données de référence
---------------------
Le module de gestion des données de référence contient plusieurs types de données reliées entre elles par une ontologie.


index:: vocabulaires

Données de référence : vocabulaires
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Les vocabulaires sont des listes de termes organisées. Elles peuvent prendre la forme de listes simples (on parle alors de liste d'autorités) ou de listes hiérarchiques (on parle alors de thésaurus).

Il est possible de créer des vocabulaires mais également d'en importer.

Pour créer un vocabulaire il suffit de cliquer sur l'icône + sur la page d'accueil ou sur l'onglet **vocabulaire**.

.. image:: ../images/creationVoc.png
   :height: 616px
   :width: 767px
   :scale: 60%
   :alt: capture d'écran de la page de création de vocabulaire
   :align: center

Vous devez donner à ce vocabulaire : 

- un titre : le nom du vocabulaire
- une description : que contient ce vocabulaire
- autorité nommante ARK : le rattacher à une autorité d'archivage

A ce stade vous avez la possibilité d'importer un fichier contenant les termes que vous souhaitez gérer dans l'application.

2 syntaxes sont possibles : 

- format de fichier simple : un tableur numérique (excel ou calc par exemple) ne contenant qu'une colonne
- format de fichier linked csv : un tableur avec une syntaxe particulière permettant l'import de thésaurus hiérarchiques

.. note::

Une section de la documentation détaille plus précisément la syntaxe attendue. 

Voir .._import-linkedcsv

Une fois créé, le vocabulaire se voit attribué un identifiant unique. Vous pouvez ensuite créer des concepts en cliquant sur le menu de gauche "ajouter concept"

.. note::

A ce stade le vocabulaire est en mode brouillon ce qui signifie qu'il ne sera pas accessible aux autres modules. Une fois publié, il est toujours possible de rajouter des termes ou de les modifier mais plus de les supprimer.

Pour ajouter un concept vous pouvez saisir :

- une définition : 
- un exemple d'utilisation
- un ou plusieurs libellés 
- sélectionner un type de libellé (préféré, alternatif..) et un code langue

Une fois le concept créé vous pouvez ajouter un nouveau concept au même niveau ou un sous-concept pour celui que vous venez de créér.

.. image:: ../images/creationConcept.png
   :height: 382px
   :width: 806px
   :scale: 50%
   :alt: capture d'écran de la page de création de concept
   :align: center
   
index:: organization_unit

Données de référence : autorités administratives
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Les autorités administratives et leur entités associées (unités administratives et agents) sont des acteurs du système d'archivage électronique
au sens du SEDA. Les unités administratives notamment se voient attibuer un rôle archivistique comme service versant, service producteur, service d'archive etc...

Ensuite, les modules du SAEM étant synchronisés, ces unités administratives et leurs contacts référents sont automatiquement déclarés comme intervenants et utilisateurs dans les modules gestion des processus d'archivage 
et gestion de l'archivage et de la conservation.

Ces entités pré existent par ailleurs dans chaque collectivité, en dehors de l'archivage électronique et sont déja décrits a minima dans 
les référentiels ressources humaines par exemple comme les organigrammes, les annuaires LDAP ou dans d'autres applications du sytème d'information 
d'une collectivité. 

Au sein du référentiel, une **autorité administrative** est donc une collectivité (commune, département ou un établissement public) "mère", 
composée de plusieurs **unités administratives** "filles" (directions ou services) et de x **agents** de type personne (employés). 
L'autorité administrative n'a pas de rôle archivistique propre ; les rôles sont définis pour les unités.


Dans le référentiel il est obligatoire d'associer un service d'archives à une autorité administrative lorsqu'on la créé. 
Par héritage, toutes les unités administratives de cette autorité auront le même service d'archives qui est logiquement compétent pour toute la collectivité.


- **Ci-dessous exemple de l'autorité administrative Commune de Saint-Etienne associée à son propre service d'archives.** 

Accessible via l'onglet **Autorité administrative** sur la page d'accueil du référentiel.

.. image:: ../images/Doc-SAEM-REF-OngletAA-exSaint-Etienne.PNG
   :scale: 50%
   :alt: capture d'écran de la page de d'accueil des autorités administratives
   :align: center

Plusieurs unités administratives "filles" ont été déclarées au sein de cette autorité.

Pour chaque unité administrative, il faut choisir le ou les rôle(s) archivistique(s) que celle-ci rempli dans le système.


- **Exemple de la direction des archives de la commune de Saint-Etienne**
*On constate que ce service d'archives est compétent pour toute la commune.*


.. image:: ../images/Doc-SAEM-REF-OngletUA-exSASaint-Etienne.PNG
   :scale: 50%
   :alt: capture d'écran de la page de relation avec le services d'archives
   :align: center



- **Exemple de la direction des ressources humaines de la commune de Saint-Etienne, qui est un service producteur et versant.**


.. image:: ../images/Doc-SAEM-REF-OngletUA-exSVSaint-EtienneRH.PNG
   :scale: 50%
   :alt: capture d'écran de la page de paramétrage de l'unité administrative
   :align: center

On constate que cette direction a un contact référent "M DUPUY" : c'est une personne employée par la collectivité, qui est 
présente dans l'onglet **Agent** de la commune et qui va jouer un rôle dans la préparation 
des versement d'archives électroniques de cette direction  dans le module gestion des processus d'archivage .



.. image:: ../images/Doc-SAEM-REF-OngletUA-exAgentContactReferentSVSaint-EtienneRH.PNG
   :scale: 50%
   :alt: capture d'écran de la page de création de contact référent
   :align: center


.. index:: authority_record

Données de référence : notices d'autorité
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Les notices d'autorités sont des fiches de description des producteurs d'archives, normalisées selon les principes de la norme ISAAR-CPF.
Le référentiel implémente le schéma EAC-CPF et permet ainsi d'importer, de créer et d'exporter des notices d'autorités dans un format 
intéropérable avec d'autres applications.

Dans le référentiel, chaque notice d'autorité présente des informations sur un producteur qui sont classée dans quatre permiers onglets :

- **informations générales**, 

- **description**, 

- **propriétés**, 

- **relations**.

Ces onglets correspondent globalement aux zones de la norme ISAAR-CPF et présentent les balises/champs à compléter ou ajouter.
Un cinquième onglet **cycle de vie** correspond à l'enregistrement des évenement de la notice.

**Pour qui peut-on créer une notice d'autorité ?**

- On peut créer une notice d'autorité pour une autorité administrative, ce qui permet de décrire la collectivité, ses dates extrême, son organisation et ses évolutions.


- On peut créer une notice d'autorité pour une unité administrative, ce qui permet de détailler l'organisation d'un service producteur, ses missions, ses relations hiérarchives, 
chronologiques ou d'association avec d'autres services.


- On peut créer si besoin une notice d'autorité pour un agent d'une autorité administrative : par exemple un secrétaire génértal de mairie qui verse des archives en dehors du cadre classique des versements de sa collectivité.


- On peut créer une notice d'autorité pour une personne, dont les archives privées électroniques seront versées dans le SAEM Girondin.

Dans la notice d'autorité, le nom de l'entité décrite, qu'elle soit de type personne, famille ou collectivité doit être rédigé sous la forme autorisée 
conformément à la norme AFNOR NF Z 44-060 (déc.1996) : Catalogage d’auteurs et d'anonymes – forme et structure des vedettes de collectivités
auteurs. La notice permet de déclarer des formes parallèles du nom. 

**Exemple de la notice d'autorité de la direction des ressources humaines de la ville de Saint-Etienne**


- onglet **informations générales**:


.. image:: ../images/Doc-SAEM-REF-OngletNA-exDRH-StEtne-InfosGnles.PNG
   :scale: 50%
   :alt: capture d'écran de la page de création d'une notice d'autorité
   :align: center


- onglet **description**:


.. image:: ../images/Doc-SAEM-REF-OngletNA-exDRH-StEtne-Description.PNG
   :scale: 50%
   :alt: capture d'écran de la page de création d'une notice d'autorité
   :align: center


- onglet **relations** :


.. image:: ../images/Doc-SAEM-REF-OngletNA-exDRH-StEtne-Relations.PNG
   :scale: 50%
   :alt: capture d'écran de la page de création d'une notice d'autorité
   :align: center

.. index:: seda_profile

Données de référence : profils SEDA
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Les développements du référentiel SAEM ont permis d'écrire des profil SEDA dans l'application, au lieu de passer par le logiciel AGAPE, afin de pouvoir récuperer directement les informations du profil dans les différents modules, par synchronisation.
L'écriture d'un profil SEDA dans le référentiel permet de définir les contraintes liées à un versement d'archives type.

A partir du moment où un projet de versement automatisé ou régulier d'archives électroniques est envisagé, l'étude du flux permet de définir un plan de classement type pour un versement, ainsi que le contenu attendu avec une normalisation des intitulés.
Par ailleurs l'étude permet de définir à l'avance les règles de gestion qui s'appliqueront à un versement ou aux articles qui le composent : DUA et sort final ou communicabilité par exemple.

Une fois qu'il est créé, un profil est associé à une ou plusieurs unités administratives de type service versant qui pourront l'utiliser pour faire des versements.

Un même profil peut être utilisé par différentes collectivités, par exemple un profil de versement des pièces de passation d'un marché public.

Pour associer un profil à une unité administrative qui a le rôle service versant il faut que les deux entités soient créées et que le profil soit publié.

- **Association d'un profil à une unité administrative via le bouton ajouter**
.. image:: ../images/Doc-SAEM-Association-Profil-SV.PNG



Les renseignements indiqués dans le profil plus ceux qui seront ajoutés ou captés via les métadonnées sur les archives lors du transfert constitueront les informations 
du bordereau de versement. 


Un **profil** correspond à la description d'un versement. 

**A VERIFIER PAR TESTS** Après avoir donné un nom au profil il faut constituer une unité d'archives de niveau supérieur qui servira d'enveloppe pour réunir toutes les autres unités d'archives (et leurs fichiers) en un seul et même versement.
Cette première unité d'archive doit être en cardinalité 1 ou 1-n, avec un niveau de description et une règle de restriction d'accès renseignés : si ces informations de sont pas rensignés "en dur" dans le profil, elles devront être complétées au moment du versement. 
Par ailleurs le nom de l'unité d'archives peut être écrit "en dur" ou, si on veut faire désigner le versement à partir du formulaire au moment de sa préparation, il faut indiquer comment formuler l'intitulé dans le commentaire d'aide à la saisie des informations générales du nom. 
Au niveau inférieur les **unités d'archives** correspondent aux dossiers et sous-dossiers types d'un versement.


Sous les unités d'archives on trouve les **objets-données**. 


Un objet-donnée est une description d'un document attendu dans le versement (type, format, dates etc.).


- **Exemple d'un profil pour une versement type d'un dossier de recrutement.**
*On voit à droite l'arborescence prédéfinie du profil, avec les sous dossiers qu'on s'attend à trouver dans chaque versement et, quand il sont connus à l'avance,
les fichiers indispensables ou facultatifs dans chaque sous-dossier.*

.. image:: ../images/Doc-SAEM-REF-ProfilSeda-exDossierRecrutement.PNG


- **Exemple de saisie d'informations au niveau du premier sous-dossier unité d'archives.**

.. image:: ../images/Doc-SAEM-REF-ProfilSeda-exDossierRecrutement-UD1.PNG



**Comment construire un profil dans le référentiel ?**


On part du principe qu'un document type peut être décrit de la même manière même si il est utilisé dans des contextes différents.

Exemples : 

- une "lettre de motivation" peut être présente dans un dossier "candidature" en réponse à offre de recrutement ou dans un dossier "formation" pour solliciter une autorisation

- un "ordre de mission" peut être présent dans un sous-dossier "déplacement professionnel" ou dans un dossier "formation".


De même on considère qu'un dossier ou un sous-dossier type peut être utilisé dans différents profils.

Exemples :

- un dossier type "déplacement professionnel" peut être un élément du "dossier administratif" d'un agent ou d'un dossier "colloque"
et ainsi de suite.

On part donc du plus petit dénominateur commun pour définir des entités objets-donnés, unités d'archives ou profils les plus génériques et réutilisables possibles.


En vue de la création du profil "Dossier de recrutement d'un agent dans la collectivite" on a d'abord créé des objets données individuels.

*Nota bene :* 

- pour avoir un affichage du nom type de l'objet-données (par exemple "curriculum vitae") dans l'arborescence du profil, il faut l'écrire en dur dans le champ "commentaire d'aide à la saisie" de la cardinalit de l'objet-données car cette balise nom type n'existe pas dans le SEDA.

- pour avoir plus tard l'affichage du nom type de l'objet-données dans le formulaire de saisie du module gestion des processus d'archivage (afin de guider l'utilisateur au moment des versements pour qu'il sache quel type de document associer au bon endroit), il faut l'écrire en dur dans le champ "description".


**Exemple de constitution d'un objet donnée réutilisable, exemple d'un Curriculum vitae**

.. image:: ../images/Doc-SAEM-REF-OngletUnitesArchives-ExObjetDonneeCV.PNG


Ensuite on crée de la même manière des unités d'archives qui vont constituer des sous dossiers type d'un dossier de recrutement.

Par exemple on crée l'unité **"Réponse à une offre d'emploi et pièces justificatives"**

A l'intérieur on peut **importer** des objets données nécessaires existants ou en **ajouter** en indiquant la cardinalité :

- 1 si l'objet est obligatoire et unique (1 cv ou une lettre de motication par exemple)

- 1-n si l'objet est obligatoire et peut être multiple (des pièces justificatives liées à la candidature)

- 0-1 si l'objet est facultatif et unique

- 0-n si l'objet est facultatif et peut être multiple (des lettres de rcommandation par exemple)


Enfin on crée de la même manière un profil en important des unités d'archives prédéfinies dont on peut le cas échant, avant publication du profil, modifier les propriétés (cardinalités ou règles de gestion par exemple).

Cette méthode permet de réutiliser des entités déjà définies dans un nouveau contexte, favorise la normalisation des versements et permet un travail collaboratif entre archivistes.

Données de référence : unités d'archives ou objets données 
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
Actuellement, sur la page d'accueil du référentiel il existe un onglet **Profils SEDA** et un onglet **Unités d'archives**.
L'onglet **Unités d'archives** permet de creer soit des unités d'archive, soit des objets données. 

.. image:: ../images/Doc-SAEM-REF-OngletUnitesArchivesCreation.PNG

Concernant l'utilisation des unités d'archives ou des objets données, voir les explications ci-dessus dans la section Données de référenc : Profils SEDA
