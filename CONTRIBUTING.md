Pour contribuer à cette documentation il est possible d'ajouter des éléments sur les fichiers existants ou d'en créer de nouveaux.
Les règles de syntaxe sont les suivantes

============
Titre 1
============

Titre 2
-------------------

.. note::
Une note


Titre 3
~~~~~~~~~~~~~~~ 

Titre 4
===============

Publication
============

Les éléments publiés sont automatiquement transmis vers le site readthedoc.io et transformés en code HTML
La documentation générée est également disponible aux format epub et pdf depuis le site readthedoc

